from PIL import Image
import numpy as np
from numpy import pi, sin, cos
import random

import GraphicUtils

FOX_OBJ_FILE_NAME = "fox.obj"


class Coordinate3D:
    def __init__(self, x=0., y=0., z=0.):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return str(self.to_array())

    def to_array(self):
        return np.array([self.x, self.y, self.z])  # dtype?


class Coordinate2D:
    def __init__(self, x=0., y=0.):
        self.x = x
        self.y = y


class Pixel3:
    def __init__(self, r=0, g=0, b=0):
        self.r = r  # 0 - 255
        self.g = g
        self.b = b
        # self.coordinate = Coordinate3D()

    def __str__(self):
        # return "[" + str(self.r) + ", " + str(self.g) + ", " + str(self.b) + "]"
        return str(self.to_array())

    def to_array(self):
        return np.array([self.r, self.g, self.b], dtype=np.uint8)

    # def set_coordinate(self, coordinate: Coordinate3D):
    #     self.coordinate = coordinate


CYAN_COLOR = Pixel3(51, 204, 204)
DEFAULT_LINE_COLOR = CYAN_COLOR


class Model3D:
    def __init__(self):
        self.points = []
        self.polygons = []

    def __str__(self):
        str_points = self.points.copy()
        i = 0
        for _ in self.points:
            str_points[i] = str(self.points[i])
            i += 1

        return str(str_points)

    def get_points(self):
        return self.points

    def get_polygons(self):
        return self.polygons

    def open_points_from_obj_file(self, filename):
        with open(filename) as file:
            for line in file:
                line = line.split()
                if line[0] == "v":
                    self.points.append(Coordinate3D(float(line[1]), float(line[2]), float(line[3])))

    def open_polygons_from_obj_file(self, filename):
        with open(filename) as file:
            for line in file:
                line = line.split()
                if line[0] == "f":
                    v1 = int(line[1].split("/")[0]) - 1  # в obj вершины нумеруются с единицы
                    v2 = int(line[2].split("/")[0]) - 1
                    v3 = int(line[3].split("/")[0]) - 1
                    self.polygons.append([v1, v2, v3])

    def open_from_obj_file(self, filename):
        self.open_points_from_obj_file(filename)
        self.open_polygons_from_obj_file(filename)


class MyImage:
    def __init__(self, H, W):
        self.H = H      # x space size
        self.W = W      # y space size
        self.image_matrix = np.array([[Pixel3() for _ in range(W)] for _ in range(H)])
        self.point_of_view = [1, 0, 0]
        self.zbuff = np.array([[float('inf') for _ in range(W)] for _ in range(H)])

    def __str__(self):
        str_image_matrix = self.image_matrix.copy()
        b = 0
        r = 0
        for row in self.image_matrix:
            for _ in row:
                str_image_matrix[r][b] = str(self.image_matrix[r][b])
                b += 1

            b = 0
            r += 1
        return str(str_image_matrix)

    def __to_array(self):
        arr_image_matrix = np.zeros([self.H, self.W, 3], dtype=np.uint8)
        b = 0
        r = 0
        for row in self.image_matrix:
            for _ in row:
                arr_image_matrix[r][b] = self.image_matrix[r][b].to_array()
                b += 1

            b = 0
            r += 1
        return arr_image_matrix

    def get_width(self):
        return self.W

    def get_height(self):
        return self.H

    def set_pixel(self, x: int, y: int, color=DEFAULT_LINE_COLOR):
        self.image_matrix[x][y] = color

    def get_pixel(self, x, y):
        return self.image_matrix[x][y]

    def save(self, filename):
        im3 = Image.fromarray(self.__to_array(), mode="RGB")
        im3.save(filename)

    def draw_line_dt(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        for t in np.arange(0, 1, 0.01):
            x = int(x0 * (1.-t) + x1 * t)
            y = int(y0 * (1.-t) + y1 * t)
            self.set_pixel(x, y, color)

    def draw_line_each_x(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        for x in range(int(x0), int(x1)):
            t = (x-x0) / float(x1 - x0)
            y = int(y0 * (1.-t) + y1 * t)
            self.set_pixel(x, y, color)

    def draw_line_working_version(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        steep = False
        if np.fabs(x0 - x1) < np.fabs(y0 - y1):
            x0, y0 = y0, x0
            x1, y1 = y1, x1
            steep = True

        if x0 > x1:     # make it left-to-right
            x0, x1 = x1, x0
            y0, y1 = y1, y0

        for x in range(int(x0), int(x1+1)):
            t = (x-x0) / float(x1 - x0)
            y = int(y0 * (1.-t) + y1 * t)
            if steep:
                self.set_pixel(y, x, color)
            else:
                self.set_pixel(x, y, color)

    def draw_line_bresenham(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        steep = False
        if np.fabs(x0 - x1) < np.fabs(y0 - y1):
            x0, y0 = y0, x0
            x1, y1 = y1, x1
            steep = True

        if x0 > x1:  # make it left-to-right
            x0, x1 = x1, x0
            y0, y1 = y1, y0

        dx = x1 - x0
        dy = y1 - y0
        derror = np.fabs(dy / float(dx))
        error = 0
        y = y0
        for x in range(int(x0), int(x1+1)):
            if steep:
                self.set_pixel(round(y), round(x), color)
            else:
                self.set_pixel(round(x), round(y), color)
            error += derror
            if error > .5:
                if y1 > y0:
                    y += 1
                else:
                    y -= 1
                error -= 1.

    def draw_line_bresenham_from_points(self, point1: Coordinate3D, point2: Coordinate3D, color=DEFAULT_LINE_COLOR):
        x0 = 5 * point1.y + 500    # здесь берем y и z, а не x и y!
        y0 = 5 * point1.z + 500
        x1 = 5 * point2.y + 500
        y1 = 5 * point2.z + 500
        self.draw_line_bresenham(x0, y0, x1, y1, color)

    def draw_wireframe(self, model: Model3D):
        points = model.get_points()
        polygons = model.get_polygons()

        for polygon in polygons:
            point1 = points[polygon[0]]
            point2 = points[polygon[1]]
            point3 = points[polygon[2]]

            self.draw_line_bresenham_from_points(point1, point2)
            self.draw_line_bresenham_from_points(point2, point3)
            self.draw_line_bresenham_from_points(point3, point1)

    def _get_pixels_set(self, xmin, ymin, xmax, ymax):
        x_set = [x for x in range(round(xmin), round(xmax))]  # по идее можно не округлять
        y_set = [y for y in range(round(ymin), round(ymax))]
        pixels = []
        for x in x_set:
            for y in y_set:
                pixels.append(Coordinate2D(x, y))
        return pixels

    def _chose_color(self, polygon_cos):
        return Pixel3(polygon_cos*255, polygon_cos*255, polygon_cos*255)

    def draw_polygons(self, model: Model3D):
        for polygon in model.get_polygons():
            #random_color = Pixel3(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

            point0_before_transfer = model.get_points()[polygon[0]]
            point1_before_transfer = model.get_points()[polygon[1]]
            point2_before_transfer = model.get_points()[polygon[2]]

            normal = GraphicUtils.find_polygon_normal(point0_before_transfer, point1_before_transfer,
                                                      point2_before_transfer)
            cos_angle = (np.dot(normal, self.point_of_view)) / \
                        (np.linalg.norm(normal) * np.linalg.norm(self.point_of_view))
            # print(np.dot(normal, self.point_of_view))
            # print(np.linalg.norm(normal))
            # print(np.linalg.norm(self.point_of_view))

            if cos_angle < 0:      # Если полигон направлен от наблюдателя, то не рисуем его
                continue    # todo может быть наоборот?

            polygon_color = self._chose_color(cos_angle)

            # Расчитываем область интереса
            x0 = 5 * model.get_points()[polygon[0]].y + 500   # здесь берем y и z, а не x и y!
            x1 = 5 * model.get_points()[polygon[1]].y + 500
            x2 = 5 * model.get_points()[polygon[2]].y + 500
            y0 = 5 * model.get_points()[polygon[0]].z + 500
            y1 = 5 * model.get_points()[polygon[1]].z + 500
            y2 = 5 * model.get_points()[polygon[2]].z + 500

            xmin = min(x0, x1, x2)
            ymin = min(y0, y1, y2)
            xmax = max(x0, x1, x2)
            ymax = max(y0, y1, y2)
            if xmin < 0: xmin = 0
            if ymin < 0: ymin = 0
            if xmax > self.get_height() - 1: xmax = self.get_height()
            if ymax > self.get_width() - 1: ymax = self.get_width()

            pixels_to_checking_location = self._get_pixels_set(xmin, ymin, xmax, ymax)
            inner_pixels = []
            for pixel in pixels_to_checking_location:
                lambda0, lambda1, lambda2 = GraphicUtils.find_barycentric_coordinates(int(pixel.x), int(pixel.y), x0, y0, x1, y1, x2, y2)
                if lambda0 > 0 and lambda1 > 0 and lambda2 > 0:
                    z = lambda0 * point0_before_transfer.x + lambda1 * point1_before_transfer.x + lambda2 * point2_before_transfer.x
                    if self.zbuff[pixel.x, pixel.y] > z:
                        self.zbuff[pixel.x, pixel.y] = z
                        self.set_pixel(int(pixel.x), int(pixel.y), polygon_color)
                        inner_pixels.append(pixel)


def rotate180(filename):
    im = Image.open(filename)

    out = im.rotate(180)
    out.save(filename)


# Task 1
def hello_world():
    H = 200
    W = 200

    black_matrix = np.zeros([H, W], dtype=np.uint8)
    white_matrix = np.zeros([H, W], dtype=np.uint8) + 255
    print(black_matrix)
    print(white_matrix)

    im = Image.fromarray(black_matrix, mode="L")
    im.save("black.png")

    im2 = Image.fromarray(white_matrix, mode="L")
    im2.save("white.png")

    red_pixel = np.array([255, 0, 0], dtype=np.uint8)
    red_matrix = np.zeros([H, W, 3], dtype=np.uint8) + red_pixel
    # print(red_matrix)
    im3 = Image.fromarray(red_matrix, mode="RGB")
    im3.save("red.png")

    gradient_matrix = np.zeros([H, W, 3], dtype=np.uint8)
    r = 0
    b = 0
    for row in gradient_matrix:
        for item in row:
            item = item + np.array([r % 256, 0, b % 256], dtype=np.uint8)
            gradient_matrix[r][b] = item
            b += 1
        b = 0
        r += 1
    im4 = Image.fromarray(gradient_matrix, mode="RGB")
    im4.save("gradient.png")


# Task 2
def classes_test():
    image = MyImage(60, 60)
    print(image)
    image.save("image.png")


# Task 3
def lines_test():
    image1 = MyImage(200, 200)
    draw_star(image1, image1.draw_line_dt)
    image1.save("line_dt_star.png")

    image2 = MyImage(200, 200)
    draw_star(image2, image2.draw_line_each_x)
    image2.save("line_each_x.png")

    image3 = MyImage(200, 200)
    draw_star(image3, image3.draw_line_working_version)
    image3.save("line_working_version.png")

    image4 = MyImage(200, 200)
    draw_star(image4, image4.draw_line_bresenham)
    image4.save("line_bresenham.png")


def draw_star(image: MyImage, draw_line_function: MyImage.__dict__):
    x0 = image.get_height() // 2    # точно ли здесь высота, а не длинна?
    y0 = image.get_width() // 2
    color = Pixel3(100, 100, 100)
    for i in range(13):    # (линии из точки (100,100) в точки (100 + 95 𝑐𝑜𝑠 (𝛼) ,100 + 95 𝑠𝑖𝑛 (𝛼) ), 𝛼 =2𝜋𝑖/13, 𝑖 = 0,...,12.
        alpha = (2 * np.pi * i) / 13
        x1 = 100 + 95 * cos(alpha)
        y1 = 100 + 95 * sin(alpha)
        draw_line_function(x0, y0, x1, y1, color)


# Task 4 and Task 5
def model_test():
    model = Model3D()
    model.open_points_from_obj_file(FOX_OBJ_FILE_NAME)
    print(model)

    image = MyImage(1000, 1000)
    for point in model.points:
        x = int(5 * point.y + 500)
        y = int(5 * point.z + 500)

        # print(x, y)
        image.set_pixel(x, y, CYAN_COLOR)

    image.save("fox_points.png")


# Task 6 and Task 7
def wireframe_test():
    image = MyImage(1000, 1000)
    model = Model3D()
    model.open_points_from_obj_file(FOX_OBJ_FILE_NAME)
    model.open_polygons_from_obj_file(FOX_OBJ_FILE_NAME)
    print("Model:")
    print("points:", model.points)
    print("polygons:", model.polygons)
    image.draw_wireframe(model)
    image.save("fox_wireframe.png")
    rotate180("fox_wireframe.png")


# Task 8 - 15
def polygons_test():
    image = MyImage(1000, 1000)
    model = Model3D()
    model.open_points_from_obj_file(FOX_OBJ_FILE_NAME)
    model.open_polygons_from_obj_file(FOX_OBJ_FILE_NAME)
    image.draw_polygons(model)
    image.save("fox_polygons.png")
    rotate180("fox_polygons.png")


if __name__ == '__main__':
    # hello_world()
    # classes_test()
    # lines_test()
    # model_test()
    # wireframe_test()
    polygons_test()
