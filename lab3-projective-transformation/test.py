import numpy as np
from lab3 import Coordinates3D

ax = 6000
ay = 6000
u0 = 500
v0 = 250

t = np.array([0.005, -0.045, 15.0]).T
K = np.array([[ax, 0, u0], [0, ay, v0], [0, 0, 1]])
print(K.dot(t))


def _get_plain_transferred_coordinates(point: Coordinates3D):
    x = 6000 * point.x + 500
    y = 6000 * point.y + 250
    z = 6000 * point.z + 500
    return Coordinates3D(x, y, z)


def _get_projective_transferred_coordinates(point: Coordinates3D):
    ax = 6000
    ay = 6000
    u0 = 500
    v0 = 500

    np_point = np.array([point.x, point.y, point.z]).T

    t = np.array([0.005, -0.045, 1.0]).T
    coordinates = np_point + t
    print(coordinates)
    K = np.array([[ax, 0, u0], [0, ay, v0], [0, 0, 1]])
    [u, v, z] = K.dot(coordinates)

    return Coordinates3D(u, v, z)


if __name__ == '__main__':
    point = Coordinates3D(-0.046146, 0.050437, 0.002961)
    plain_point = _get_plain_transferred_coordinates(point)
    projective_point = _get_projective_transferred_coordinates(point)
    print("plain:", plain_point)
    print("projective:", projective_point)


