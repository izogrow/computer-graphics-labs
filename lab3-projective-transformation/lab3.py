from PIL import Image
import numpy as np
from numpy import pi, sin, cos
import random

import GraphicUtils

OBJ_FILE_NAME = "Test.obj"


class Coordinates3D:
    def __init__(self, x=0., y=0., z=0.):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return str(self.to_array())

    def to_array(self):
        return np.array([self.x, self.y, self.z])  # dtype?


class Coordinates2D:
    def __init__(self, x=0., y=0.):
        self.x = x
        self.y = y


class Pixel3:
    def __init__(self, r=0, g=0, b=0):
        self.r = r  # 0 - 255
        self.g = g
        self.b = b
        # self.coordinate = Coordinates3D()

    def __str__(self):
        # return "[" + str(self.r) + ", " + str(self.g) + ", " + str(self.b) + "]"
        return str(self.to_array())

    def to_array(self):
        return np.array([self.r, self.g, self.b], dtype=np.uint8)

    # def set_coordinate(self, coordinate: Coordinates3D):
    #     self.coordinate = coordinate


CYAN_COLOR = Pixel3(51, 204, 204)
DEFAULT_LINE_COLOR = CYAN_COLOR


class Model3D:
    def __init__(self):
        self.points = []
        self.polygons = []

    def __str__(self):
        str_points = self.points.copy()
        i = 0
        for _ in self.points:
            str_points[i] = str(self.points[i])
            i += 1

        return str(str_points)

    def get_points(self):
        return self.points

    def get_polygons(self):
        return self.polygons

    def open_points_from_obj_file(self, filename):
        with open(filename) as file:
            for line in file:
                line = line.split()
                if line[0] == "v":
                    self.points.append(Coordinates3D(float(line[1]), float(line[2]), float(line[3])))

    def open_polygons_from_obj_file(self, filename):
        with open(filename) as file:
            for line in file:
                line = line.split()
                if line[0] == "f":
                    v1 = int(line[1].split("/")[0]) - 1  # в obj вершины нумеруются с единицы
                    v2 = int(line[2].split("/")[0]) - 1
                    v3 = int(line[3].split("/")[0]) - 1
                    self.polygons.append([v1, v2, v3])

    def open_from_obj_file(self, filename):
        self.open_points_from_obj_file(filename)
        self.open_polygons_from_obj_file(filename)

    def rotate(self, x_angle, y_angle, z_angle):
        first = np.array([[1, 0, 0], [0, cos(x_angle), sin(x_angle)], [0, -sin(x_angle), cos(x_angle)]])
        second = np.array([[cos(y_angle), 0, sin(y_angle)], [0, 1, 0], [-sin(y_angle), 0, cos(y_angle)]])
        third = np.array([[cos(z_angle), sin(z_angle), 0], [-sin(z_angle), cos(z_angle), 0], [0, 0, 1]])
        R = first.dot(second).dot(third)

        rotated_points = []
        for point in self.get_points():
            np_point = np.array([point.x, point.y, point.z]).T
            np_rotated_point = R.dot(np_point)
            rotated_points.append(Coordinates3D(np_rotated_point[0], np_rotated_point[1], np_rotated_point[2]))

        self.points = rotated_points


class MyImage:
    def __init__(self, H, W):
        self.H = H      # x space size
        self.W = W      # y space size
        self.image_matrix = np.array([[Pixel3() for _ in range(W)] for _ in range(H)])
        self.point_of_view = [0, 0, 1]
        self.zbuff = np.array([[float('inf') for _ in range(W)] for _ in range(H)])

    def __str__(self):
        str_image_matrix = self.image_matrix.copy()
        b = 0
        r = 0
        for row in self.image_matrix:
            for _ in row:
                str_image_matrix[r][b] = str(self.image_matrix[r][b])
                b += 1

            b = 0
            r += 1
        return str(str_image_matrix)

    def __to_array(self):
        arr_image_matrix = np.zeros([self.H, self.W, 3], dtype=np.uint8)
        b = 0
        r = 0
        for row in self.image_matrix:
            for _ in row:
                arr_image_matrix[r][b] = self.image_matrix[r][b].to_array()
                b += 1

            b = 0
            r += 1
        return arr_image_matrix

    def get_width(self):
        return self.W

    def get_height(self):
        return self.H

    def set_pixel(self, x: int, y: int, color=DEFAULT_LINE_COLOR):
        self.image_matrix[x][y] = color

    def get_pixel(self, x, y):
        return self.image_matrix[x][y]

    def save(self, filename):
        im3 = Image.fromarray(self.__to_array(), mode="RGB")
        im3.save(filename)

    def draw_line_dt(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        for t in np.arange(0, 1, 0.01):
            x = int(x0 * (1.-t) + x1 * t)
            y = int(y0 * (1.-t) + y1 * t)
            self.set_pixel(x, y, color)

    def draw_line_each_x(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        for x in range(int(x0), int(x1)):
            t = (x-x0) / float(x1 - x0)
            y = int(y0 * (1.-t) + y1 * t)
            self.set_pixel(x, y, color)

    def draw_line_working_version(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        steep = False
        if np.fabs(x0 - x1) < np.fabs(y0 - y1):
            x0, y0 = y0, x0
            x1, y1 = y1, x1
            steep = True

        if x0 > x1:     # make it left-to-right
            x0, x1 = x1, x0
            y0, y1 = y1, y0

        for x in range(int(x0), int(x1+1)):
            t = (x-x0) / float(x1 - x0)
            y = int(y0 * (1.-t) + y1 * t)
            if steep:
                self.set_pixel(y, x, color)
            else:
                self.set_pixel(x, y, color)

    def draw_line_bresenham(self, x0, y0, x1, y1, color=DEFAULT_LINE_COLOR):
        steep = False
        if np.fabs(x0 - x1) < np.fabs(y0 - y1):
            x0, y0 = y0, x0
            x1, y1 = y1, x1
            steep = True

        if x0 > x1:  # make it left-to-right
            x0, x1 = x1, x0
            y0, y1 = y1, y0

        dx = x1 - x0
        dy = y1 - y0
        derror = np.fabs(dy / float(dx))
        error = 0
        y = y0
        for x in range(int(x0), int(x1+1)):
            if steep:
                self.set_pixel(round(y), round(x), color)
            else:
                self.set_pixel(round(x), round(y), color)
            error += derror
            if error > .5:
                if y1 > y0:
                    y += 1
                else:
                    y -= 1
                error -= 1.

    def draw_line_bresenham_from_points(self, point1: Coordinates3D, point2: Coordinates3D, color=DEFAULT_LINE_COLOR):
        point1_transferred = self.get_transferred_coordinates(point1)
        point2_transferred = self.get_transferred_coordinates(point2)

        x0 = point1_transferred.x    # здесь берем y и z, а не x и y!
        y0 = point1_transferred.y
        x1 = point2_transferred.x
        y1 = point2_transferred.y
        self.draw_line_bresenham(x0, y0, x1, y1, color)

    def draw_wireframe(self, model: Model3D):
        points = model.get_points()
        polygons = model.get_polygons()

        for polygon in polygons:
            point1 = points[polygon[0]]
            point2 = points[polygon[1]]
            point3 = points[polygon[2]]

            self.draw_line_bresenham_from_points(point1, point2)
            self.draw_line_bresenham_from_points(point2, point3)
            self.draw_line_bresenham_from_points(point3, point1)

    def _get_pixels_set(self, xmin, ymin, xmax, ymax):
        x_set = [x for x in range(int(xmin), int(xmax) + 1)]  # по идее можно не округлять
        y_set = [y for y in range(int(ymin), int(ymax) + 1)]
        pixels = []
        for x in x_set:
            for y in y_set:
                pixels.append(Coordinates2D(x, y))
        return pixels

    def _chose_color_basic(self, polygon_cos):
        return Pixel3(polygon_cos*255, polygon_cos*255, polygon_cos*255)

    def _get_plain_transferred_coordinates(self, point: Coordinates3D):
        x = 6000 * point.x + 500
        y = 6000 * point.y + 250
        z = 6000 * point.z + 500
        return Coordinates3D(x, y, z)

    def _get_projective_transferred_coordinates(self, point: Coordinates3D):
        ax = 6000
        ay = 6000
        u0 = 750 * 2
        v0 = 750 * 2

        np_point = np.array([point.x, point.y, point.z]).T
        t = np.array([0.005, -0.045, 0.25]).T
        coordinates = np_point + t
        K = np.array([[ax, 0, u0], [0, ay, v0], [0, 0, 1]])
        [u, v, z] = K.dot(coordinates)

        return Coordinates3D(u, v, z)

    def get_transferred_coordinates(self, point: Coordinates3D, is_projective_required=True):
        if is_projective_required:
            return self._get_projective_transferred_coordinates(point)
        else:
            return self._get_plain_transferred_coordinates(point)

    def draw_polygons(self, model: Model3D, is_projective_required=True, is_basic_shading=True):
        for polygon in model.get_polygons():
            point0_before_transfer = model.get_points()[polygon[0]]
            point1_before_transfer = model.get_points()[polygon[1]]
            point2_before_transfer = model.get_points()[polygon[2]]

            normal = GraphicUtils.find_polygon_normal(point0_before_transfer, point1_before_transfer,
                                                      point2_before_transfer)
            cos_angle = (np.dot(normal, self.point_of_view)) / \
                        (np.linalg.norm(normal) * np.linalg.norm(self.point_of_view))

            if cos_angle < 0:      # Если полигон направлен от наблюдателя, то не рисуем его.
                continue    # Может быть наоборот?

            if is_basic_shading:
                polygon_color = self._chose_color_basic(cos_angle)
            else:   # random color
                polygon_color = Pixel3(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

            # Расчитываем область интереса
            point0_after_transfer = self.get_transferred_coordinates(point0_before_transfer, is_projective_required=is_projective_required)
            point1_after_transfer = self.get_transferred_coordinates(point1_before_transfer, is_projective_required=is_projective_required)
            point2_after_transfer = self.get_transferred_coordinates(point2_before_transfer, is_projective_required=is_projective_required)

            x0 = point0_after_transfer.x
            x1 = point1_after_transfer.x
            x2 = point2_after_transfer.x
            y0 = point0_after_transfer.y
            y1 = point1_after_transfer.y
            y2 = point2_after_transfer.y

            xmin = min(x0, x1, x2)
            ymin = min(y0, y1, y2)
            xmax = max(x0, x1, x2)
            ymax = max(y0, y1, y2)
            if xmin < 0: xmin = 0
            if ymin < 0: ymin = 0
            if xmax > self.get_height() - 1: xmax = self.get_height()
            if ymax > self.get_width() - 1: ymax = self.get_width()

            pixels_to_checking_location = self._get_pixels_set(xmin, ymin, xmax, ymax)
            inner_pixels = []
            for pixel in pixels_to_checking_location:
                lambda0, lambda1, lambda2 = GraphicUtils.find_barycentric_coordinates(int(pixel.x), int(pixel.y), x0, y0, x1, y1, x2, y2)
                if lambda0 > 0 and lambda1 > 0 and lambda2 > 0:
                    z = lambda0 * point0_before_transfer.z + lambda1 * point1_before_transfer.z + lambda2 * point2_before_transfer.z
                    try:
                        if self.zbuff[pixel.x, pixel.y] > z:
                            self.zbuff[pixel.x, pixel.y] = z
                            self.set_pixel(int(pixel.x), int(pixel.y), polygon_color)
                            inner_pixels.append(pixel)
                    except IndexError:
                        continue


def rotate90(filename):
    im = Image.open(filename)

    out = im.rotate(90)
    out.save(filename)


# Task 1
def hello_world():
    H = 200
    W = 200

    black_matrix = np.zeros([H, W], dtype=np.uint8)
    white_matrix = np.zeros([H, W], dtype=np.uint8) + 255
    print(black_matrix)
    print(white_matrix)

    im = Image.fromarray(black_matrix, mode="L")
    im.save("black.png")

    im2 = Image.fromarray(white_matrix, mode="L")
    im2.save("white.png")

    red_pixel = np.array([255, 0, 0], dtype=np.uint8)
    red_matrix = np.zeros([H, W, 3], dtype=np.uint8) + red_pixel
    # print(red_matrix)
    im3 = Image.fromarray(red_matrix, mode="RGB")
    im3.save("red.png")

    gradient_matrix = np.zeros([H, W, 3], dtype=np.uint8)
    r = 0
    b = 0
    for row in gradient_matrix:
        for item in row:
            item = item + np.array([r % 256, 0, b % 256], dtype=np.uint8)
            gradient_matrix[r][b] = item
            b += 1
        b = 0
        r += 1
    im4 = Image.fromarray(gradient_matrix, mode="RGB")
    im4.save("gradient.png")


# Task 2
def classes_test():
    image = MyImage(60, 60)
    print(image)
    image.save("image.png")


# Task 3
def lines_test():
    image1 = MyImage(200, 200)
    draw_star(image1, image1.draw_line_dt)
    image1.save("line_dt_star.png")

    image2 = MyImage(200, 200)
    draw_star(image2, image2.draw_line_each_x)
    image2.save("line_each_x.png")

    image3 = MyImage(200, 200)
    draw_star(image3, image3.draw_line_working_version)
    image3.save("line_working_version.png")

    image4 = MyImage(200, 200)
    draw_star(image4, image4.draw_line_bresenham)
    image4.save("line_bresenham.png")


def draw_star(image: MyImage, draw_line_function: MyImage.__dict__):
    x0 = image.get_height() // 2    # точно ли здесь высота, а не длинна?
    y0 = image.get_width() // 2
    color = Pixel3(100, 100, 100)
    for i in range(13):    # (линии из точки (100,100) в точки (100 + 95 𝑐𝑜𝑠 (𝛼) ,100 + 95 𝑠𝑖𝑛 (𝛼) ), 𝛼 =2𝜋𝑖/13, 𝑖 = 0,...,12.
        alpha = (2 * np.pi * i) / 13
        x1 = 100 + 95 * cos(alpha)
        y1 = 100 + 95 * sin(alpha)
        draw_line_function(x0, y0, x1, y1, color)


# Task 4 and Task 5
def points_test():
    model = Model3D()
    model.open_points_from_obj_file(OBJ_FILE_NAME)

    image = MyImage(1000, 1000)
    for point in model.points:
        x = int(5000 * point.x + 500)
        y = int(5000 * point.y + 500)

        # print(x, y)
        image.set_pixel(x, y, CYAN_COLOR)

    image.save("obj_points.png")
    print("Points from model have been rendered")


# Task 6 and Task 7
def wireframe_test():
    image = MyImage(1000, 1000)
    model = Model3D()
    model.open_points_from_obj_file(OBJ_FILE_NAME)
    model.open_polygons_from_obj_file(OBJ_FILE_NAME)
    # print("Model:")
    # print("points:", model.points)
    # print("polygons:", model.polygons)
    image.draw_wireframe(model)
    image.save("obj_wireframe.png")
    rotate90("obj_wireframe.png")
    print("Wireframe model has been rendered.")


# Task 8 - 15
def polygons_test():
    image = MyImage(1000, 1000)
    model = Model3D()
    model.open_points_from_obj_file(OBJ_FILE_NAME)
    model.open_polygons_from_obj_file(OBJ_FILE_NAME)
    image.draw_polygons(model, is_projective_required=False)
    image.save("obj_polygons.png")
    rotate90("obj_polygons.png")
    print("Polygons model has been rendered.")


# Task 16
def projective_transformation_test():
    image = MyImage(1000, 1000)
    model = Model3D()
    model.open_points_from_obj_file(OBJ_FILE_NAME)
    model.open_polygons_from_obj_file(OBJ_FILE_NAME)
    image.draw_polygons(model)
    image.save("obj_projective_polygons.png")
    rotate90("obj_projective_polygons.png")
    print("Projective polygons model has been rendered.")


# Task 17
def rotating_test():
    image = MyImage(1000, 1000)
    model = Model3D()
    model.open_points_from_obj_file(OBJ_FILE_NAME)
    model.open_polygons_from_obj_file(OBJ_FILE_NAME)
    model.rotate(0, 90, 0)
    image.draw_polygons(model)
    image.save("obj_projective_rotated_polygons.png")
    rotate90("obj_projective_rotated_polygons.png")
    print("Projective rotated polygons model has been rendered.")


if __name__ == '__main__':
    # hello_world()
    # classes_test()
    # lines_test()
    # points_test()
    # wireframe_test()
    # polygons_test()
    # projective_transformation_test()
    rotating_test()
