from PIL import Image
import numpy as np


def find_barycentric_coordinates(x: int, y: int, x0: float, y0: float, x1: float, y1: float, x2: float, y2: float):
    lambda0 = ((x1 - x2) * (y - y2) - (y1 - y2) * (x - x2)) / ((x1 - x2) * (y0 - y2) - (y1 - y2) * (x0 - x2))
    lambda1 = ((x2 - x0) * (y - y0) - (y2 - y0) * (x - x0)) / ((x2 - x0) * (y1 - y0) - (y2 - y0) * (x1 - x0))
    lambda2 = ((x0 - x1) * (y - y1) - (y0 - y1) * (x - x1)) / ((x0 - x1) * (y2 - y1) - (y0 - y1) * (x2 - x1))
    return lambda0, lambda1, lambda2


def find_polygon_normal(point0, point1, point2):
    normal = np.cross([point1.x - point0.x, point1.y - point0.y, point1.z - point0.z],
                    [point1.x - point2.x, point1.y - point2.y, point1.z - point2.z])
    return normal


def test_barycentric_coordinates():
    x = 10
    y = 10
    x0 = 1
    y0 = 1
    x1 = 1
    y1 = 20
    x2 = 20
    y2 = 10

    barycentric_coordinates = find_barycentric_coordinates(x, y, x0, y0, x1, y1, x2, y2)
    print(barycentric_coordinates)
    print(sum(barycentric_coordinates))


if __name__ == '__main__':
    test_barycentric_coordinates()
